Rails.application.routes.draw do
  resources :events
  resources :themes
  resources :details
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :members
  resources :abouts
  resources :contacts
  resources :tutorials
  resources :homes

  root 'homes#index'
end

class AboutsController < ApplicationController
  before_action :set_about, only: [:show, :edit, :update, :destroy]

  def index
    @abouts = About.all
  end

  def show
    @about = About.find(params[:id])
    @abouts = About.order('created_at desc').limit(4).offset(1)
  end

  def new
    @about = About.new
  end

  def create
    @about = About.new(about_params)

    respond_to do |format|
      if @about.save
        format.html { redirect_to @about, notice: 'About was successfully created.' }
        format.json { render :show, status: :created, location: @about }
      else
        format.html { flash[:failure!] = 'About Not created! Verify Details & Send Again!' }
        format.html { render 'new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @about.update(about_params)
        format.html { redirect_to @about, notice: 'About was successfully updated.' }
        format.json { render :show, status: :ok, location: @about }
      else
        format.html { render :edit }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @about.destroy
    respond_to do |format|
      format.html { redirect_to abouts_url, notice: 'About was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_about
      @about = About.find(params[:id])
    end

    def about_params
      params.require(:about).permit(:title, :body)
    end
end

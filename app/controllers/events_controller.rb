class EventsController < InheritedResources::Base
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def index
    @events = Event.all
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html do
          flash[:success] = 'Successfully Done'
          redirect_to events_url
        end
      else
        format.html { flash[:failure!] = 'Error! Verify Details!' }
        format.html { render 'new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html do
          flash[:success] = 'Successfully Updated'
          redirect_to events_url
        end
      else
        format.html { flash[:failure!] = 'Error! Verify Details!' }
        format.html { render 'new' }
      end
    end
  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to event_url, notice: 'Successfully Destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:username, :email, :confirm)
    end
end

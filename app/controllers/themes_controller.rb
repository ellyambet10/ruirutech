class ThemesController < InheritedResources::Base

  private

    def theme_params
      params.require(:theme).permit(:title, :demo_link, :download_link)
    end

end

class Tutorial < ApplicationRecord
  validates :title, presence: true
  validates :body, presence: true
  validates :code, presence: true
  validates :video, presence: true
end

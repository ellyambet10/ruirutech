class Event < ApplicationRecord
  validates :username, uniqueness: true, presence: true
  validates :email, uniqueness: true, presence: true, format: /\A[^@]+@[^@]+\z/
end

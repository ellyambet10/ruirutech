class Member < ApplicationRecord
  validates :username, presence: true
  validates :email, uniqueness: true, presence: true, format: /\A[^@]+@[^@]+\z/
end

class Home < ApplicationRecord
  validates :event, presence: true
  validates :date_time, presence: true
  validates :venue, presence: true
  validates :body, presence: true
end

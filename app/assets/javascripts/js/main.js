// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// When the user clicks anywhere outside of the modal, close it
var modal = document.getElementById('ticketModal');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function bigImg(x) {
  x.style.width = "230px";
}

function normalImg(x) {
  x.style.width = "200px";
}

function joinWhatsappgroup() {
  var txt;
  if (confirm("Join Our whatsapp Group?")) {
    window.location.href = "https://chat.whatsapp.com/JHoCR6n5nSlLpsIkBNrnCm";
  } else {
    txt = "You Cancelled!!!";
  }
  document.getElementById("demo").innerHTML = txt;
}

function followTwitter() {
  var txt;
  if (confirm("Follow Us On Twitter?")) {
    window.location.href = "https://twitter.com/RuiruTech";
  } else {
    txt = "You Cancelled!!!";
  }
  document.getElementById("demo").innerHTML = txt;
}

json.extract! tutorial, :id, :title, :body, :code, :video, :created_at, :updated_at
json.url tutorial_url(tutorial, format: :json)

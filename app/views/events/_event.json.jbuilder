json.extract! event, :id, :username, :email, :confirm, :created_at, :updated_at
json.url event_url(event, format: :json)

json.extract! home, :id, :event, :date_time, :venue, :body, :created_at, :updated_at
json.url home_url(home, format: :json)

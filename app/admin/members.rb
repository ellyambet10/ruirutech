ActiveAdmin.register Member do
  permit_params :username, :email

  show do |_t|
    attributes_table do
      row :username
      row :email
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :username
      f.input :email
    end
    f.actions
  end
end

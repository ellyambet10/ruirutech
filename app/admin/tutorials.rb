ActiveAdmin.register Tutorial do
  permit_params :title, :body, :code, :video

  show do |_t|
    attributes_table do
      row :title
      row :body
      row :code
      row :video
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :title
      f.input :body
      f.input :code
      f.input :video
    end
    f.actions
  end
end

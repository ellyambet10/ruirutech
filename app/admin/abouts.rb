ActiveAdmin.register About do
  permit_params :title, :body

  show do |_t|
    attributes_table do
      row :title
      row :body
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :title
      f.input :body
    end
    f.actions
  end
end

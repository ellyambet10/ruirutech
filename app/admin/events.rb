ActiveAdmin.register Event do
  permit_params :username, :email, :confirm

  show do |_t|
    attributes_table do
      row :username
      row :email
      row :confirm
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :username
      f.input :email
      f.input :confirm
    end
    f.actions
  end
end

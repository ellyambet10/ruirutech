ActiveAdmin.register Home do
  permit_params :event, :date_time, :venue, :body

  show do |_t|
    attributes_table do
      row :event
      row :date_time
      row :venue
      row :body
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :event
      f.input :date_time
      f.input :venue
      f.input :body
    end
    f.actions
  end
end

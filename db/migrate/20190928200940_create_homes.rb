class CreateHomes < ActiveRecord::Migration[5.2]
  def change
    create_table :homes do |t|
      t.string :event
      t.datetime :date_time
      t.string :venue
      t.text :body

      t.timestamps
    end
  end
end

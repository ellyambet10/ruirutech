class CreateThemes < ActiveRecord::Migration[5.2]
  def change
    create_table :themes do |t|
      t.string :title
      t.string :demo_link
      t.string :download_link

      t.timestamps
    end
  end
end

class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :username
      t.string :email
      t.boolean :confirm

      t.timestamps
    end
  end
end
